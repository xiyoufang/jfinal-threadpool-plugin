package com.jfplugin.threadpool;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	ThreadPoolPlugin plugin = new ThreadPoolPlugin(8, 32, 64, 1);
    	plugin.start();
    	for(int i = 0 ; i < 64 ; i++){
	    	ThreadPoolKit.execute(new Thread(){
	    		public void run(){
	    			System.out.println(Thread.currentThread().getName()+"-->A");	
	    			try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
	    		}
	    	});
    	}
    	new ThreadPoolPlugin("test",8, 32, 64, 1).start();
    	for(int i = 0 ; i < 64 ; i++){
	    	ThreadPoolKit.use("test").execute(new Thread(){
	    		public void run(){
	    			System.out.println(Thread.currentThread().getName()+"-->A");	
	    			try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
	    		}
	    	});
    	}
        assertTrue( true );
    }
}
